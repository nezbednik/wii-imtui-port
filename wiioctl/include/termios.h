#ifndef _SYS_TERMIOS_H
#define _SYS_TERMIOS_H

#define ICANON 0
#define ECHO 0
#define ICRNL 0

#define VSUSP 0
#define VSTOP 0
#define VSTART 0
#define _POSIX_VDISABLE 0

struct termios {
    int c_iflag;
    int c_lflag;
    char c_cc[1];
};

int tcgetattr(int fd, struct termios* p);

#define TCSANOW 0

int tcsetattr(int fd, int offset, struct termios* p);

#endif
