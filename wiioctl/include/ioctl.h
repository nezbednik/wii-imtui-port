#ifndef _SYS_IOCTL_H
#define _SYS_IOCTL_H

struct winsize {
    int ws_row;
    int ws_col;
};

#define TIOCGWINSZ 0

int ioctl(int fd, int cmd, struct winsize* ws);

#endif
