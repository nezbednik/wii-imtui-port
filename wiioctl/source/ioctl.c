#include "../include/ioctl.h"

int _ws_row = 29;
int _ws_col = 77;

int ioctl(int fd, int cmd, struct winsize* ws) {
    ws->ws_row = _ws_row;
    ws->ws_col = _ws_col;
}

int wiioctl_setxy(int x, int y) {
    if (y != -1) _ws_row = y;
    if (x != -1) _ws_col = x;
}
