cmake_minimum_required(VERSION 3.26.4)
project(wiioctl DESCRIPTION "Shim for various C headers on the Wii")

set(CMAKE_SYSTEM_NAME NintendoWii)
set(CMAKE_BUILD_TYPE Release)
set(CMAKE_VERBOSE_MAKEFILE ON)

add_library(wiioctl STATIC source/termios.c source/ioctl.c source/signal.c source/conio.c)

install(TARGETS wiioctl)
install(FILES include/termios.h include/ioctl.h DESTINATION include/wiioctl/sys)
install(FILES include/conio.h include/wiioctl.h DESTINATION include/wiioctl)
