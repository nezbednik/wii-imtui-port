# wii-imtui-port

Largely unfinished port of ImTui & PDCursesMod from 10/2023. Might not build.\
PDCursesMod depends on `wiioctl`, a "compatibility layer" library for the Wii that emulates Windows & Linux system headers, depending on the context.

### Build order
- `wiioctl`
- `wii-pdcursesmod`
- `wii-imtui`
