pkgname="wii-imtui"
pkgver="1.0.5"
pkgrel="1"
pkgdesc="Immediate Mode Text-based User Interface C++ Library"
arch=("any")
url="https://imtui.ggerganov.com"
license=("MIT")
source=("https://github.com/ggerganov/imtui/releases/download/v$pkgver/imtui-$pkgver.tar.gz")
depends=("devkitPPC" "wii-cmake")
sha256sums=("82677597065b7e8c738db5677dde9126a9367b0a15345334ee41eba50692d064")
options=("!strip")

_find_tool() {
    export "TOOL=$(find "/var/chroot/archlinux/opt/devkitpro/$1" "/opt/devkitpro/$1" 2>/dev/null | head -n1)"
    "$TOOL" $2 2>&1 >/dev/null
    if [ $? -ne 0 ]; then
        echo "could not find $1" >&2
        exit 1
    else
        echo "$TOOL"
    fi
}

build() {
    export "CMAKE=$(_find_tool portlibs/wii/bin/powerpc-eabi-cmake --version)"
    
    echo "cmake_minimum_required(VERSION 3.26.4)" > "CMakeLists.txt"
    echo "project(- NONE)" >> "CMakeLists.txt"
    echo "message(\${CMAKE_INSTALL_PREFIX})" >> "CMakeLists.txt"
    export "PREFIX=$("$CMAKE" . 2>&1 | head -n 1)"
    echo "$PREFIX" > "$srcdir/prefix.txt"

    cd imtui-*/

    # patch ImTui to use curses.h
    cd src
    sed -i "/#define KEY_/d; s/#ifdef _WIN32/#if 1/; s/pdcurses.h/curses.h/" imtui-impl-ncurses.cpp
    sed -i "s/ungetch/PDC_ungetch/" imtui-impl-ncurses.cpp
    cd ..

    mkdir -p build
    cd build

    "$CMAKE" \
     -DCMAKE_CXX_FLAGS="-I\"$PREFIX/include\"" \
     -DIMTUI_BUILD_EXAMPLES=0 \
     -DCMAKE_VERBOSE_MAKEFILE=1 \
     -DMINGW=1 \
     ..

    make
}

package() {
    cd imtui-*/build

    "$(_find_tool devkitPPC/bin/powerpc-eabi-strip --version)" --strip-unneeded src/libimtui.a src/libimtui-ncurses.a third-party/libimgui-for-imtui.a

    make DESTDIR="$pkgdir" install
    cp -v third-party/libimgui-for-imtui.a "${pkgdir}$(cat "$srcdir/prefix.txt")/lib"

    if [ -d "$pkgdir/var/chroot/archlinux" ]; then
        mv "$pkgdir/var/chroot/archlinux/opt" "$pkgdir"
        rmdir "$pkgdir/var/chroot/archlinux" "$pkgdir/var/chroot" "$pkgdir/var"
    fi
}
