#include <stdio.h>
#include <stdlib.h>
#include <gccore.h>
#include <wiiuse/wpad.h>

/*
#include <imtui/imtui.h>
#include <imtui/imtui-impl-ncurses.h>
*/

extern "C" {
    static void __console_drawc(int c);
}

int main(int argc, char **argv) {
	VIDEO_Init();
	WPAD_Init();
	GXRModeObj* rmode = VIDEO_GetPreferredMode(NULL);
	void* xfb = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));
	console_init(xfb,20,20,rmode->fbWidth,rmode->xfbHeight,rmode->fbWidth*VI_DISPLAY_PIX_SZ);
	VIDEO_Configure(rmode);
	VIDEO_SetNextFramebuffer(xfb);
	VIDEO_SetBlack(FALSE);
	VIDEO_Flush();
	VIDEO_WaitVSync();
	if(rmode->viTVMode&VI_NON_INTERLACE) VIDEO_WaitVSync();

        for (int i = 0; i < 29; i++) {
            for (int j = 0; j < (i == 28 ? 76 : 77); j++) {
                printf(i == 0 ? "0" : "=");
            }
        }

        __console_drawc('D');

        while(1) {WPAD_ScanPads();if (WPAD_ButtonsDown(0) & WPAD_BUTTON_A) break;VIDEO_WaitVSync();}

/*
        IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    auto screen = ImTui_ImplNcurses_Init(true);
    ImTui_ImplText_Init();

    //bool demo = true;
    int nframes = 0;
    float fval = 1.23f;

    while (true) {
        ImTui_ImplNcurses_NewFrame();
        ImTui_ImplText_NewFrame();

        ImGui::NewFrame();

        ImGui::SetNextWindowPos(ImVec2(4, 27), ImGuiCond_Once);
        ImGui::SetNextWindowSize(ImVec2(50.0, 10.0), ImGuiCond_Once);
        ImGui::Begin("Hello, world!");
        ImGui::Text("NFrames = %d", nframes++);
        ImGui::Text("Mouse Pos : x = %g, y = %g", ImGui::GetIO().MousePos.x, ImGui::GetIO().MousePos.y);
        ImGui::Text("Time per frame %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
        ImGui::Text("Float:");
        ImGui::SameLine();
        ImGui::SliderFloat("##float", &fval, 0.0f, 10.0f);

#ifndef __EMSCRIPTEN__
        ImGui::Text("%s", "");
        if (ImGui::Button("Exit program", { ImGui::GetContentRegionAvail().x, 2 })) {
            break;
        }
#endif

        ImGui::End();

        //ImTui::ShowDemoWindow(&demo);

        ImGui::Render();

        ImTui_ImplText_RenderDrawData(ImGui::GetDrawData(), screen);
        ImTui_ImplNcurses_DrawScreen();
    }

    ImTui_ImplText_Shutdown();
    ImTui_ImplNcurses_Shutdown();
*/

        while(1) {WPAD_ScanPads();if (WPAD_ButtonsDown(0) & WPAD_BUTTON_HOME) break;VIDEO_WaitVSync();}

	return 0;
}
